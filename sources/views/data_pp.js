import { JetView } from "webix-jet";
import { data_pembelian } from "../models/records_pembelian";
import { data_penjualan } from "../models/records_penjualan";



export default class DataView extends JetView {

	config() {
		var res = webix.promise.defer();
		var grid1 = {
			view: "datatable",
			id: "tbl_pembelian",
			// autoheight: true,
			scroll: false,
		};
		var grid2 = {
			view: "datatable",
			id: "tbl_penjualan",
			// autoheight: true,
			scroll: false,

		};
		switch (this.app.config.size) {
			case "small":
				grid1.columns = [
					{ id: "id", header: "ID", css: "rank", width: 50 },
					{ id: "nama", header: "Nama" },
					{ id: "jumlah", header: "Jumlah", width: 80 },
					{ id: "harga", header: "Harga", width: 100 }
				],
				grid2.columns = [
					{ id: "id", header: "ID", css: "rank", width: 50 },
					{ id: "nama", header: "Nama" },
					{ id: "jumlah", header: "Jumlah", width: 80 },
					{ id: "harga", header: "Harga", width: 100 }
				]
				break;
			case "wide":
				grid1.columns = [
					{ id: "id", header: "ID", css: "rank", width: 50 },
					{ id: "nama", header: "Nama",fillspace: true },
					{ id: "jumlah", header: "Jumlah", width: 80 },
					{ id: "harga", header: "Harga", width: 100 }
				],
				grid2.columns = [
					{ id: "id", header: "ID", css: "rank", width: 50 },
					{ id: "nama", header: "Nama",fillspace: true },
					{ id: "jumlah", header: "Jumlah", width: 80 },
					{ id: "harga", header: "Harga", width: 100 }
				]
				break;
		}

		// fetch(data_pembelian).then(function () {
		// 	res.resolve(grid1).then(function (success) {
		// setTimeout(function () {
		// 	fetch(data_penjualan).then(function () {
		// 		res.resolve(grid2)
		// 	}, 2000)
		// });
		// 	});

		// })
		return {

			rows: [
				{ view: "label", label: "Data Pembelian" },
				grid1,

				{ view: "label", label: "Data Penjualan" },
				grid2

			], minWidth: 250
		};
		// return view;
	}
	init() {
		data_pembelian.waitData.then(() => {
			$$("tbl_pembelian").sync(data_pembelian);

			data_penjualan.waitData.then(() => {
				$$("tbl_penjualan").sync(data_penjualan);
			})
		});

		
	};

	// async init() {
	// 	try{
	// 		await data_pembelian.waitData(() => {
	// 			$$("tbl_pembelian").sync(data_pembelian);
	
	// 		data_penjualan.waitData(() => {
	// 				$$("tbl_penjualan").sync(data_penjualan);
	// 			})
	// 		});
	// 	}
	// 	catch(err){
	// 		console.log(err)
	// 	}	
	// };
}
