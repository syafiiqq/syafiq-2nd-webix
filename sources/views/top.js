import { JetView, plugins } from "webix-jet";



export default class TopView extends JetView {
	config() {
		switch(this.app.config.size){
            case "small":
				menu = {
					view: "menu", id: "top:menu",
					css: "app_menu",
					width: 1, layout: "y", select: true,
					template: "<span class='webix_icon #icon#'></span> #value# ",
					data: [
						{ value: "Data", id: "data_pp", icon: "wxi-columns" },
					]
				}
                break;
            default:
				var menu = {
					view: "menu", id: "top:menu",
					css: "app_menu",
					width: 180, layout: "y", select: true,
					template: "<span class='webix_icon #icon#'></span> #value# ",
					data: [
						{ value: "Data", id: "data_pp", icon: "wxi-columns" },
					]
				};
                break;
        }
		var header = {
			type: "header", template: "Syafiq's app", css: "webix_header app_header"
		};



		var ui = {
			type: "space", paddingX: 5, css: "app_layout", cols: [
				{ paddingX: 5, paddingY: 10, rows: [{ css: "webix_shadow_medium", rows: [header, menu] }] },
				{
					type: "wide", paddingY: 10, paddingX: 5, rows: [
						{ $subview: true }
					]
				}
			]
		};

	

    
		return ui;
	}
	init() {
		this.use(plugins.Menu, "top:menu");
	
    }
	}
