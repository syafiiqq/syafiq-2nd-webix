import "./styles/app.css";
import {JetApp, EmptyRouter, HashRouter } from "webix-jet";

export default class MyApp extends JetApp{
	constructor(config){
		const defaults = {
			id 		: APPNAME,
			version : VERSION,
			router 	: BUILD_AS_MODULE ? EmptyRouter : HashRouter,
			debug 	: !PRODUCTION,
			start 	: "/top/data_pp"
		};

		super({ ...defaults, ...config });
	}
}

webix.ready(() => {
    const app = new JetApp({
        id 		: APPNAME,
        version : VERSION,
        router 	: BUILD_AS_MODULE ? EmptyRouter : HashRouter,
        debug 	: !PRODUCTION,
        start 	: "/top/data_pp"
    });

    const size =  () => document.body.offsetWidth > 800 ? "wide" : "small";
    app.config.size = size();
    webix.event(window, "resize", function(){
        var newSize = size();
        if (newSize != app.config.size){
            app.config.size = newSize;
            app.refresh();
        }
    });

    app.render();
});