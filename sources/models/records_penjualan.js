export const data_penjualan = new webix.DataCollection({ data:[
	{ id:1, nama:"Data Pembelian", jumlah:1994, votes:678790, harga:9.2, rank:1},
	{ id:2, nama:"The Godfather", jumlah:1972, votes:511495, harga:9.2, rank:2},
	{ id:3, nama:"The Godfather: Part II", jumlah:1974, votes:319352, harga:9.0, rank:3},
	{ id:4, nama:"The Good, the Bad and the Ugly", jumlah:1966, votes:213030, harga:8.9, rank:4},
	{ id:5, nama:"My Fair Lady", jumlah:1964, votes:533848, harga:8.9, rank:5},
	{ id:6, nama:"12 Angry Men", jumlah:1957, votes:164558, harga:8.9, rank:6}
]});